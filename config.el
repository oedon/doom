(setq display-time-24hr-format t)
(display-time-mode 1)

(setq calendar-date-style 'european
      calendar-mark-holidays-flag nil
      calendar-week-start-day 1
      calendar-mark-diary-entries-flag nil)
(map! :leader
      :desc "Org Calendar"
      "o c" #'cfw:open-org-calendar)

;; (use-package! org-caldav
;;   :defer t
;;   :init
;;   (setq org-caldav-url "https://caldav.fastmail.com/dav/calendars/user/newton@fastmail.com/"
;;         org-caldav-inbox nil
;;         org-caldav-calendar-id nil
;;         org-caldav-calendars
;;          '((:calendar-id "2b563bb1-5acb-4e92-aff0-5ed25faf9086"
;;             :inbox "~/Documents/org/Personal.org"))
;;         org-caldav-delete-org-entries 'always
;;         org-caldav-delete-calendar-entries 'never))

(use-package! german-holidays
  :load-path "~/.config/doom/lisp/")
(setq calendar-holidays holiday-german-RP-holidays)

(use-package! dired
  :custom ((dired-listing-switches "-agho --group-directories-first"))
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-single-up-directory
    "l" 'dired-single-buffer))

(map! :leader
      :desc "Dired"
      "d d" #'dired
      :leader
      :desc "Dired jump to current"
      "d j" #'dired-jump
      (:after dired
        (:map dired-mode-map
         :leader
         :desc "Peep-dired image previews"
         "d p" #'peep-dired
         :leader
         :desc "Dired view file"
         "d v" #'dired-view-file)))
(evil-define-key 'normal peep-dired-mode-map (kbd "j") 'peep-dired-next-file
                                             (kbd "k") 'peep-dired-prev-file)
(add-hook 'peep-dired-hook 'evil-normalize-keymaps)
(setq peep-dired-cleanup-on-disable t)
(setq large-file-warning-threshold 50000000)

(setq
doom-font (font-spec :family "FiraCode Nerd Font" :size 18)
doom-variable-pitch-font (font-spec :family "FiraCode Nerd Font" :size 18)
doom-big-font (font-spec :family "FiraCode Nerd Font" :size 24))

;;(setq doom-theme 'doom-one)
(setq doom-theme 'doom-gruvbox)
;; (setq doom-theme 'doom-dracula)

(setq
default-directory "~/Documents/"
projectile-project-search-path '("~/Documents/org/"))

(use-package! org
  :after org
  :init
  (setq org-directory "~/Documents/org/")
  ;;(setq org-id-locations-file "~/Documents/org/")
  ;;(setq org-agenda-files '("~/org/"))
  ;;(setq org-agenda-files (list org-directory))
  (setq org-agenda-files (ignore-errors (directory-files +org-dir t "\\.org$" t)))
  ;;(setq org-agenda-files (directory-files-recursively "~/org/" "\\.org$"))
  (setq org-hide-emphasis-markers t)
  ;;(setq org-log-done 'time)
  ;;(setq org-log-done 'note)
  (setq org-tags-column 70)
  (setq org-todo-keywords '((sequence
                             "TODO(t)"
                             "NEXT(n)"
                             "REVIEW(r)"
                             "WIP(p)"
                             "WAIT(w)"
                             "EXAM(e)"
                             "FIX(f)"
                             "|"
                             "DONE(d)"
                             "CANCELLED(c)" )))

  (setq org-todo-keyword-faces
      '(("TODO" .       (:foreground "orange" :weight bold))
        ("REVIEW" .     (:foreground "turquoise" :weight bold))
        ("WIP" .        (:foreground "violet" :weight bold))
        ("WAIT" .       (:foreground "yellow" :weight bold))
        ("EXAM" .       (:foreground "red" :weight bold))
        ("FIX" .        (:foreground "red" :weight bold))
        ("DONE" .       (:foreground "forest green" :weight bold))))

  (setq org-ellipsis " ▾")
  (setq org-agenda-span 90)
  (setq org-agenda-show-future-repeats nil)
  (setq org-agenda-show-all-dates nil)
  (setq org-agenda-hide-tags-regexp "noexport")
  (setq org-use-tag-inheritance nil)

  (setq org-agenda-custom-commands
     '(("d" "Dashboard"
       ((agenda "" ((org-deadline-warning-days 14) (org-agenda-span 30)))
        (todo "READY"
          ((org-agenda-overriding-header "Ready Tasks")))
        (tags-todo "agenda/ACTIVE" ((org-agenda-overriding-header "Active Projects")))))

      ("n" "Next Tasks"
       ((todo "READY"
          ((org-agenda-overriding-header "Ready Tasks")))))

      ("W" "Work Tasks" tags-todo "+work-email")

      ("w" "Workflow Status"
       ((todo "WAITING"
              ((org-agenda-overriding-header "Waiting on External")
               (org-agenda-files org-agenda-files)))

        (todo "REVIEW"
              ((org-agenda-overriding-header "In Review")
               (org-agenda-files org-agenda-files)))
        (todo "VISIT"
              ((org-agenda-overriding-header "VISIT")
               (org-agenda-todo-list-sublevels nil)
               (org-agenda-files org-agenda-files)))
        (todo "ON HOLD"
              ((org-agenda-overriding-header "Project Backlog")
               (org-agenda-todo-list-sublevels nil)
               (org-agenda-files org-agenda-files)))
        (todo "READY"
              ((org-agenda-overriding-header "Ready for Work")
               (org-agenda-files org-agenda-files)))
        (todo "WIP"
              ((org-agenda-overriding-header "Active Projects")
               (org-agenda-files org-agenda-files)))
        (todo "CHECK"
              ((org-agenda-overriding-header "Completed Projects")
               (org-agenda-files org-agenda-files)))
        (todo "CANC"
              ((org-agenda-overriding-header "Cancelled Projects")
               (org-agenda-files org-agenda-files)))))))

  (setq org-capture-templates
      `(("t" "Tasks / Projects")
        ("tt" "Task" entry (file+olp "~/Documents/org/agenda.org" "Inbox")
             "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)

        ("j" "Journal Entries")
        ("jj" "Journal" entry
             (file+olp+datetree "~/Documents/org/Journal.org")
             "\n* %<%I:%M %p> - Journal :journal:\n\n%?\n\n"
             ;; ,(dw/read-file-as-string "~/Notes/Templates/Daily.org")
             :clock-in :clock-resume
             :empty-lines 1)
        ("jm" "Meeting" entry
             (file+olp+datetree "~/Documents/org/Journal.org")
             "* %<%I:%M %p> - %a :meetings:\n\n%?\n\n"
             :clock-in :clock-resume
             :empty-lines 1)

        ("w" "Workflows")
        ("we" "Checking Email" entry (file+olp+datetree "~/Documents/org/Journal.org")
             "* Checking Email :email:\n\n%?" :clock-in :clock-resume :empty-lines 1)

        ("m" "Metrics Capture")
        ("mw" "Weight" table-line (file+headline "~/Documents/org/Metrics.org" "Weight")
         "| %U | %^{Weight} | %^{Notes} |" :kill-buffer t)))



)
  ;(setq org-ellipsis "⤵")

;; Test hl-todo insted of todo keyword faces
 ;; (setq hl-todo-keyword-faces
 ;;       '(("TODO"   . "#FF0000")
 ;;         ("FIXME"  . "#FF0000")
 ;;         ("DEBUG"  . "#A020F0")
 ;;         ("GOTCHA" . "#FF4500")
 ;;         ("STUB"   . "#1E90FF")))

(use-package! org-super-agenda
  :after org-agenda
  :init
  (setq org-super-agenda-groups '((:name "Today"
                                   :time-grid t
                                   :scheduled today)
                                  (:name "Due today"
                                   :deadline today)
                                  (:name "Important"
                                   :priority "A")
                                  (:name "Overdue"
                                   :deadline past)
                                  (:name "Due soon"
                                   :deadline future)))
  :config
  (org-super-agenda-mode))

(use-package! org-bullets
  :after org
  :hook
  (org-mode . org-bullets-mode)
  :init
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

;; Set minor bullets
(font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
(font-lock-add-keywords 'org-journal-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

(use-package! org-fancy-priorities
  :after org
  :hook
  (org-mode . org-fancy-priorities-mode)
  :config
  (setq org-fancy-priorities-list '("⚡" "☕" "C")))

(defun my/style-org()
  (set-face-attribute 'italic nil
                      :foreground "#ff6f00")
  (set-face-attribute 'bold nil
                      :foreground "#a19afc"
                      :weight 'bold)
  (set-face-attribute 'org-link nil
                      :weight 'normal
                      :background nil)
  (set-face-attribute 'org-code nil
                      :foreground "#a9a1e1"
                      :background nil)
  (set-face-attribute 'org-date nil
                      ;;:foreground "#36c9cf"
                     ;:foreground "#5B6268"
                      :background nil)
  (set-face-attribute 'org-level-1 nil
                     ;; :foreground "steelblue2"
                      :background nil
                      :height 1.2
                      :weight 'normal)
  (set-face-attribute 'org-level-2 nil
                      ;;:foreground "slategray2"
                      :background nil
                      :height 1.1
                      :weight 'normal)
  (set-face-attribute 'org-level-3 nil
                      ;;:foreground "SkyBlue2"
                      :background nil
                      :height 1.05
                      :weight 'normal)
  (set-face-attribute 'org-level-4 nil
                      ;;:foreground "DodgerBlue2"
                      :background nil
                      :height 1.0
                      :weight 'normal)
  (set-face-attribute 'org-level-5 nil
                      :weight 'normal)
  (set-face-attribute 'org-level-6 nil
                      :weight 'normal)
  (set-face-attribute 'org-document-title nil
                      ;;:foreground "SlateGray1"
                      :background nil
                      :height 1.75
                      :weight 'bold))
(add-hook 'org-mode-hook 'my/style-org)

;; TODO V2 Config In Progress
(use-package! org-roam
  :after org
  :hook
  (org-roam-mode . visual-line-mode)
  :commands
  (org-roam-buffer
   org-roam-setup
   org-roam-capture
   org-roam-node-find)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n i" . org-roam-node-insert-immediate)
         ("C-c n f" . org-roam-node-find))
  :config
  (setq org-roam-directory "~/Documents/org/org-roam/")
  (org-roam-db-autosync-mode)
  (setq org-roam-dailies-directory "journal/")
  (setq org-roam-completion-everywhere t)
  ;; FIXME Seems like this is not needed anymore
  ;;(org-id-update-id-locations (org-roam--list-all-files))
  (add-to-list 'display-buffer-alist
             '("^\\*org-roam\\*"
               (display-buffer-in-direction)
               (direction . right)
               (window-width . 0.33)
               (window-height . fit-window-to-buffer)))
  (org-roam-setup))

;; TODO Map Keys
(map! :leader
      :desc "Org Roam Buffer"
      "r b" #'org-roam-buffer-toggle)
(map! :leader
      :desc "Org Roam Insert Node"
      "r i" #'org-roam-node-insert)
(map! :leader
      :desc "Org Roam Find Node"
      "r f" #'org-roam-node-find)
(map! :leader
      :desc "Org Roam Capture Today"
      "r c" #'org-roam-dailies-capture-today)
(map! :leader
      :desc "Org Roam Goto Today"
      "r t" #'org-roam-dailies-goto-today)
(map! :leader
      :desc "Org Roam Capture New"
      "r n" #'org-roam-capture)

;; FIXME Delete after some time, this is the warning for V2
(setq org-roam-v2-ack t)

;; Immediate Note insert
(defun org-roam-node-insert-immediate (arg &rest args)
  (interactive "P")
  (let ((args (cons arg args))
        (org-roam-capture-templates (list (append (car org-roam-capture-templates)
                                                  '(:immediate-finish t)))))
    (apply #'org-roam-node-insert args)))

(use-package! org-journal
  :after org
  :custom
  (org-journal-date-prefix "#+title: ")
  (org-journal-file-format "%Y-%m-%d.org")
  (org-journal-dir "~/Documents/org/journal/")
  (org-journal-date-format "%Y-%m-%d"))

(use-package! org-download
  :after org
  :init
  :config
  (setq-default org-download-image-dir "~/Documents/org/img/")
  (setq org-image-actual-width 750)
  (setq org-startup-with-inline-images t))

(use-package! deft
  :after org
  :init
  :custom
  (deft-directory "~/Documents/org/org-roam/")
  (deft-default-extension "org")
  (deft-recursive t))

;;(setq org-latex-toc-command "\\tableofcontents \\clearpage")
(setq reftex-default-bibliography "~/Documents/LaTeX/Bibliography/ref.bib")
(setq bibtex-completion-bibliography "~/Documents/LaTeX/Bibliography/ref.bib")

(with-eval-after-load 'ox-latex
(add-to-list 'org-latex-classes
             '("org-plain-latex"
               "\\documentclass{article}
           [NO-DEFAULT-PACKAGES]
           [PACKAGES]
           [EXTRA]"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(setq display-line-numbers-type t)
(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))
;;(global-set-key "\C-x\ t" 'toggle-truncate-lines)

(add-hook! 'rainbow-mode-hook
  (hl-line-mode (if rainbow-mode -1 +1)))

(defun prefer-horizontal-split ()
  (set-variable 'split-height-threshold nil t)
  (set-variable 'split-width-threshold 40 t)) ; make this as low as needed
(add-hook 'markdown-mode-hook 'prefer-horizontal-split)

;; LSP-Python-ms is only for NixOS because emacs can't compile the binary.
;; (after! lsp-python-ms
;;   (setq lsp-python-ms-executable (executable-find "python-language-server"))
;;   (set-lsp-priority! 'mspyls 1))

;; Pyvenv is only for NixOS to create the pip packages in the project.
(use-package pyvenv
  :config
  (pyvenv-mode t)

  ;; Set correct Python interpreter
  (setq pyvenv-post-activate-hooks
        (list (lambda ()
                (setq python-shell-interpreter (concat pyvenv-virtual-env "bin/python3")))))
  (setq pyvenv-post-deactivate-hooks
        (list (lambda ()
                (setq python-shell-interpreter "python3")))))

;; (use-package! langtool
;;   :load-path "/home/oedon/.config/doom/lisp"
;;   :config
;;   (setq langtool-language-tool-jar "/home/oedon/.config/LanguageTool-5.1/languagetool-commandline.jar"))

(setq company-global-modes '(not eshell-mode))

(use-package! beancount
  ;;:load-path "~/.config/doom/lisp/beancount-mode/"
  :init
  :config
  (require 'beancount)
  (add-to-list 'auto-mode-alist '("\\.beancount\\'" . beancount-mode))
  :custom
  (beancount-number-alignment-column 70))
(add-hook 'beancount-mode-hook 'outline-minor-mode)
(add-hook 'beancount-mode-hook 'org-bullets-mode)
(add-hook 'beancount-mode-hook 'my/style-org)
(evil-define-key 'normal beancount-mode-map (kbd "TAB") 'beancount-outline-cycle)

(setq rmh-elfeed-org-files (list "~/Documents/org/elfeed.org"))
(add-hook! 'elfeed-search-mode-hook 'elfeed-update)
(after! elfeed
  (setq elfeed-search-filter "@1-month-ago +unread"))

(use-package! rate-sx
  :load-path "~/.config/doom/lisp/")

;; (defun efs/exwm-update-class ()
;;   (exwm-workspace-rename-buffer exwm-class-name))

;; (use-package exwm
;;   :config
;;   ;; Set the default number of workspaces
;;   (setq exwm-workspace-number 5)

;;   ;; When window "class" updates, use it to set the buffer name
;;   ;; (add-hook 'exwm-update-class-hook #'efs/exwm-update-class)

;;   ;; These keys should always pass through to Emacs
;;   (setq exwm-input-prefix-keys
;;     '(?\C-x
;;       ?\C-u
;;       ?\C-h
;;       ?\M-x
;;       ?\M-`
;;       ?\M-&
;;       ?\M-:
;;       ?\C-\M-j  ;; Buffer list
;;       ?\C-\ ))  ;; Ctrl+Space

;;   ;; Ctrl+Q will enable the next key to be sent directly
;;   (define-key exwm-mode-map [?\C-q] 'exwm-input-send-next-key)

;;   ;; Set up global key bindings.  These always work, no matter the input state!
;;   ;; Keep in mind that changing this list after EXWM initializes has no effect.
;;   (setq exwm-input-global-keys
;;         `(
;;           ;; Reset to line-mode (C-c C-k switches to char-mode via exwm-input-release-keyboard)
;;           ([?\s-r] . exwm-reset)

;;           ;; Move between windows
;;           ([s-left] . windmove-left)
;;           ([s-right] . windmove-right)
;;           ([s-up] . windmove-up)
;;           ([s-down] . windmove-down)

;;           ;; Launch applications via shell command
;;           ([?\s-&] . (lambda (command)
;;                        (interactive (list (read-shell-command "$ ")))
;;                        (start-process-shell-command command nil command)))

;;           ;; Switch workspace
;;           ([?\s-w] . exwm-workspace-switch)

;;           ;; 's-N': Switch to certain workspace with Super (Win) plus a number key (0 - 9)
;;           ,@(mapcar (lambda (i)
;;                       `(,(kbd (format "s-%d" i)) .
;;                         (lambda ()
;;                           (interactive)
;;                           (exwm-workspace-switch-create ,i))))
;;                     (number-sequence 0 9))))

;;   (exwm-enable))

;; (require 'exwm-randr)
;; (exwm-randr-enable)

;; (start-process-shell-command "xrandr" nil "")
